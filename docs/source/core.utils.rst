core.utils package
==================

Subpackages
-----------

.. toctree::
   :maxdepth: 4

   core.utils.config

core.utils.Colors module
------------------------

.. automodule:: core.utils.Colors
   :members:
   :undoc-members:
   :show-inheritance:

core.utils.DataTypeInput module
-------------------------------

.. automodule:: core.utils.DataTypeInput
   :members:
   :undoc-members:
   :show-inheritance:

core.utils.DataTypeOutput module
--------------------------------

.. automodule:: core.utils.DataTypeOutput
   :members:
   :undoc-members:
   :show-inheritance:

core.utils.Task module
----------------------

.. automodule:: core.utils.Task
   :members:
   :undoc-members:
   :show-inheritance:

core.utils.make\_research module
--------------------------------

.. automodule:: core.utils.make_research
   :members:
   :undoc-members:
   :show-inheritance:

core.utils.signals module
-------------------------

.. automodule:: core.utils.signals
   :members:
   :undoc-members:
   :show-inheritance:

core.utils.utils module
-----------------------

.. automodule:: core.utils.utils
   :members:
   :undoc-members:
   :show-inheritance:
