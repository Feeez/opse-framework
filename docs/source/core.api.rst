core.api package
================

core.api.Api module
-------------------

.. automodule:: core.api.Api
   :members:
   :undoc-members:
   :show-inheritance:
