core.exceptions package
=======================

core.exceptions.ApiToMutchInstantiationException module
-------------------------------------------------------

.. automodule:: core.exceptions.ApiToMutchInstantiationException
   :members:
   :undoc-members:
   :show-inheritance:

core.exceptions.PreviousException module
----------------------------------------

.. automodule:: core.exceptions.PreviousException
   :members:
   :undoc-members:
   :show-inheritance:

core.exceptions.TaskInterrupt module
-----------------------------------------------

.. automodule:: core.exceptions.TaskInterrupt
   :members:
   :undoc-members:
   :show-inheritance:

core.exceptions.ToolGetProfileDuringExecutionException module
-------------------------------------------------------------

.. automodule:: core.exceptions.ToolGetProfileDuringExecutionException
   :members:
   :undoc-members:
   :show-inheritance:

core.exceptions.ToolMissDataInputException module
-------------------------------------------------

.. automodule:: core.exceptions.ToolMissDataInputException
   :members:
   :undoc-members:
   :show-inheritance:
