.. OPSE-Framework documentation master file, created by
   sphinx-quickstart on Sat May 21 16:32:48 2022.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to OPSE-Framework's documentation!
==========================================

.. toctree::
   :maxdepth: 3
   :caption: Contents:

   core

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
