core package
============

Opse module
----------------

.. automodule:: core.Opse
   :members:
   :undoc-members:
   :show-inheritance:

Subpackages
-----------

.. toctree::
   :maxdepth: 3

   core.api
   core.classes
   core.exceptions
   core.tools
   core.utils
   core.view
