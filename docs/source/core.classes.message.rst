core.classes.message package
============================

core.classes.message.Message module
-----------------------------------

.. automodule:: core.classes.message.Message
   :members:
   :undoc-members:
   :show-inheritance:
