core.classes package
====================

Subpackages
-----------

.. toctree::
   :maxdepth: 4

   core.classes.account
   core.classes.message
   core.classes.organization
   core.classes.types

core.classes.Profile module
---------------------------

.. automodule:: core.classes.Profile
   :members:
   :undoc-members:
   :show-inheritance:

core.classes.Research module
----------------------------

.. automodule:: core.classes.Research
   :members:
   :undoc-members:
   :show-inheritance:
